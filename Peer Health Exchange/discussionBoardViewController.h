//
//  discussionBoardViewController.h
//  Peer Health Exchange
//
//  Created by Tom Strissel on 2/21/14.
//  Copyright (c) 2014 Peer Health Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface discussionBoardViewController : UIViewController
- (IBAction)addQuestion:(id)sender;
@property (unsafe_unretained, nonatomic) IBOutlet UITableView *tableView;

@end
