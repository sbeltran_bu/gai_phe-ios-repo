//
//  flashCardViewController.m
//  Peer Health Exchange
//
//  Created by Tom Strissel on 2/19/14.
//  Copyright (c) 2014 Peer Health Exchange. All rights reserved.
//

#import "flashCardViewController.h"
#import "flashCard.h"

@interface flashCardViewController ()

@end

@implementation flashCardViewController
//@synthesize myData = _myData;
//@synthesize flashCardInfo;
//synthesize flashCardType;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
            }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    _counter = 0;
    flashCard *newObject1 = [[flashCard alloc] initWithAnswer:@"Sex" initWithQuestion:@"What is the best thing ever?" ];
    flashCard *newObject2 = [[flashCard alloc] initWithAnswer:@"Herpes" initWithQuestion:@"Second best thing?" ];
    flashCard *newObject3 = [[flashCard alloc] initWithAnswer:@"Condoms" initWithQuestion:@"Don't forget to buy your..." ];
    
    if ([self.myData isEqualToString:@"glbt"]){
        _flashCardType.text = @"GLBT Flashcards";
        _cardArray = [[NSMutableArray alloc]initWithObjects:newObject1, newObject2, newObject3, nil];
        _flashCardInfo.text = newObject1.definition;
        
    }
    else if ([self.myData isEqualToString:@"eatingDisorders"])
        _flashCardType.text = @"Eating Disorder Flashcards";
    else if ([self.myData isEqualToString:@"nutritionFitness"])
        _flashCardType.text = @"Nutrition Fitness Flashcards";
    else if ([self.myData isEqualToString:@"sex"])
        _flashCardType.text = @"Sex Flashcards";
    else if ([self.myData isEqualToString:@"alcoholDrugs"])
        _flashCardType.text = @"Alcohol/Drug Flashcards";
    
}
  


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)previousCard:(id)sender {
    _counter--;
    if(_counter == -1){
        _counter = [_cardArray count] - 1;
        NSLog(@"Counter: %d", _counter);
    }
    flashCard *f = _cardArray[_counter];
    _flashCardInfo.text = f.definition;
    f.isAnswer = false;
}

- (IBAction)nextCard:(id)sender {
    _counter++;
    if(_counter == [_cardArray count]){
        _counter = 0;
    }
    flashCard *f = _cardArray[_counter];
    _flashCardInfo.text = f.definition;
    f.isAnswer = false;
    
}

- (IBAction)flipCard:(id)sender {
    flashCard *f = _cardArray[_counter];
    if (!f.isAnswer) {
        _flashCardInfo.text = f.answer;
        f.isAnswer = true;
    }
    else{
        _flashCardInfo.text = f.definition;
        f.isAnswer = false;
        }
}
@end
