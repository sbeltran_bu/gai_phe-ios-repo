//
//  flashCardHomeViewController.h
//  Peer Health Exchange
//
//  Created by Tom Strissel on 2/19/14.
//  Copyright (c) 2014 Peer Health Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface flashCardHomeViewController : UIViewController
- (IBAction)alcoholDrugsPressed:(id)sender;
- (IBAction)eatingDisorderPressed:(id)sender;
- (IBAction)LGBTPressed:(id)sender;
- (IBAction)NutritionAndFitness:(id)sender;
- (IBAction)sexPressed:(id)sender;

@property bool isAlcoholDrugs;
@property bool isEatingDisorder;
@property bool isLGBT;
@property bool isNutritionFitness;
@property bool isSex;


@end