//
//  discussionBoardViewController.m
//  Peer Health Exchange
//
//  Created by Tom Strissel on 2/21/14.
//  Copyright (c) 2014 Peer Health Exchange. All rights reserved.
//

#import "discussionBoardViewController.h"

@interface discussionBoardViewController ()
@property (nonatomic) NSMutableArray *questions;

@end

@implementation discussionBoardViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.questions = [NSMutableArray new];
    

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if(cell == nil){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = self.questions[indexPath.row];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.questions.count;
}

- (IBAction)addQuestion:(id)sender {
    NSString *newString =@"Question";
    
    [_questions addObject:newString];
    [_tableView insertRowsAtIndexPaths:_questions withRowAnimation:daylight];
    
}
@end
