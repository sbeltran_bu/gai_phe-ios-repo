//
//  flashCard.m
//  Peer Health Exchange
//
//  Created by Tom Strissel on 2/20/14.
//  Copyright (c) 2014 Peer Health Exchange. All rights reserved.
//

#import "flashCard.h"

@implementation flashCard
@synthesize answer = _answer;
@synthesize definition = _definition;
@synthesize isAnswer = _isAnswer;
- (id) initWithAnswer:(NSString *) answer initWithQuestion:(NSString*) def{
    self.answer = answer;
    self.definition = def;
    
    return self;
}



@end
