//
//  flashCard.h
//  Peer Health Exchange
//
//  Created by Tom Strissel on 2/20/14.
//  Copyright (c) 2014 Peer Health Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface flashCard : NSObject

- (id) initWithAnswer:(NSString *) answer initWithQuestion:(NSString*) def;


@property NSString * answer;
@property NSString * definition;
@property bool isAnswer;
@end
