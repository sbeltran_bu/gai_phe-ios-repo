//
//  flashCardHomeViewController.m
//  Peer Health Exchange
//
//  Created by Tom Strissel on 2/19/14.
//  Copyright (c) 2014 Peer Health Exchange. All rights reserved.
//

#import "flashCardHomeViewController.h"
#import "flashCardViewController.h"

@interface flashCardHomeViewController ()

@end

@implementation flashCardHomeViewController
@synthesize isAlcoholDrugs;
@synthesize isEatingDisorder;
@synthesize isLGBT;
@synthesize isNutritionFitness;
@synthesize isSex;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)alcoholDrugsPressed:(id)sender {
    self.isAlcoholDrugs = true;
}

- (IBAction)eatingDisorderPressed:(id)sender {
    self.isEatingDisorder = true;
}

- (IBAction)LGBTPressed:(id)sender {
    self.isLGBT = true;
}

- (IBAction)NutritionAndFitness:(id)sender {
    self.isNutritionFitness = true;
}

- (IBAction)sexPressed:(id)sender {
    self.isSex = true;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"alcoholDrugs"])
    {
        flashCardViewController *dest = [segue destinationViewController];
        
        dest.myData = @"alcoholDrugs";
    }
    else if ([segue.identifier isEqualToString:@"nutritionFitness"])
    {
        flashCardViewController *dest = [segue destinationViewController];
        
        dest.myData = @"nutritionFitness";
    }
    else if ([segue.identifier isEqualToString:@"eatingDisorders"])
    {
        flashCardViewController *dest = [segue destinationViewController];
        
        dest.myData = @"eatingDisorders";
    }
    else if ([segue.identifier isEqualToString:@"glbt"])
    {
        flashCardViewController *dest = [segue destinationViewController];
        
        dest.myData = @"glbt";
    }
    else if ([segue.identifier isEqualToString:@"sex"])
    {
        flashCardViewController *dest = [segue destinationViewController];
        
        dest.myData = @"sex";
    }
}
@end

