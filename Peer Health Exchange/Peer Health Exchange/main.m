//
//  main.m
//  Peer Health Exchange
//
//  Created by Santiago Beltran on 1/29/14.
//  Copyright (c) 2014 Peer Health Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
