//
//  flashCardViewController.h
//  Peer Health Exchange
//
//  Created by Tom Strissel on 2/19/14.
//  Copyright (c) 2014 Peer Health Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface flashCardViewController : UIViewController
- (IBAction)previousCard:(id)sender;
- (IBAction)nextCard:(id)sender;
- (IBAction)flipCard:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *flashCardInfo;
@property (weak, nonatomic) IBOutlet UILabel *flashCardType;
@property NSString* myData;
@property int counter;
@property NSMutableArray* cardArray;
@end
