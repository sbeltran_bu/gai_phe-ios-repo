//
//  AppDelegate.h
//  Peer Health Exchange
//
//  Created by Santiago Beltran on 1/29/14.
//  Copyright (c) 2014 Peer Health Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
