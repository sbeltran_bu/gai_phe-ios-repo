//
//  MessagesViewController.h
//  Peer Health Exchange
//
//  Created by Heather Buckley on 2/17/14.
//  Copyright (c) 2014 Peer Health Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessagesViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *MenuButton;

@end
